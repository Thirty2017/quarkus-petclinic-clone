package org.quarkus.samples.petclinic.services;
import io.quarkus.test.Mock;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.quarkus.samples.petclinic.vet.User;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class UserServiceTest
{
    @Mock
    private EntityManager entityManager;

    @Test
    //write test on addUser
    public void testAddUser()
    {
        entityManager = Mockito.mock(EntityManager.class);
        Mockito.when(entityManager.createQuery(Mockito.anyString())).thenReturn(Mockito.mock(Query.class));
        UserService userService = new UserService();
        userService.entityManager = entityManager;
        userService.addUser("username", "test@test.com", "password");
        Mockito.verify(entityManager).persist(Mockito.any());
    }

    @Test
    public void testVerifyUser()
    {
        entityManager = Mockito.mock(EntityManager.class);
        Mockito.when(entityManager.createQuery(Mockito.anyString())).thenReturn(Mockito.mock(Query.class));
        UserService userService = new UserService();
        userService.entityManager = entityManager;
        User user = userService.verifyUser("test@test.com", "password");
        assert(user == null);
    }

    @Test
    public void testGetUser()
    {
        entityManager = Mockito.mock(EntityManager.class);
        Mockito.when(entityManager.createQuery(Mockito.anyString())).thenReturn(Mockito.mock(Query.class));
        UserService userService = new UserService();
        userService.entityManager = entityManager;
        userService.addUser("username", "test@test.com", "password");
        User user = userService.getUser("username");
        assert(user != null);
    }

    @Test
    public void testVerifyExistUser()
    {
        entityManager = Mockito.mock(EntityManager.class);
        Mockito.when(entityManager.createQuery(Mockito.anyString())).thenReturn(Mockito.mock(Query.class));
        UserService userService = new UserService();
        userService.entityManager = entityManager;
        userService.addUser("username", "test@test.com", "password");
        User user = userService.verifyUser("test@test.com", "password");
        assert(user != null);
        //password should be hashed
        assertNotEquals(user.getPassword(), "password");
    }

    @Test
    public void testVerifyFailedWhenPasswordIsIncorrect()
    {
        entityManager = Mockito.mock(EntityManager.class);
        Mockito.when(entityManager.createQuery(Mockito.anyString())).thenReturn(Mockito.mock(Query.class));
        UserService userService = new UserService();
        userService.entityManager = entityManager;
        userService.addUser("username", "test@test.com", "password");
        User user = userService.verifyUser("email", "anotherPassword");
        assert(user == null);
    }

    //throw exception when email is invalid
    @Test
    public void testAddUserWithInvalidEmail()
    {
        entityManager = Mockito.mock(EntityManager.class);
        Mockito.when(entityManager.createQuery(Mockito.anyString())).thenReturn(Mockito.mock(Query.class));
        UserService userService = new UserService();
        userService.entityManager = entityManager;
        try
        {
            userService.addUser("username", "test", "password");
        }
        catch(IllegalArgumentException e)
        {
            assert(e.getMessage().equals("Email is not valid"));
        }
    }
}
