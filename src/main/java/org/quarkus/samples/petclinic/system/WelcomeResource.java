package org.quarkus.samples.petclinic.system;
import javax.inject.Inject;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;


import io.quarkus.qute.TemplateInstance;
import org.quarkus.samples.petclinic.services.UserService;

@Path("/")
public class WelcomeResource {

    @Inject
    TemplatesLocale templates;
    @Inject
    UserService userService;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public TemplateInstance get(@CookieParam("userCookie") Cookie usernameCookie)
    {
        if(usernameCookie == null)
            return templates.welcome(null);
        var user = userService.getUser(usernameCookie.getValue());
        return templates.welcome(user);
    }
}
