package org.quarkus.samples.petclinic.services;

import org.quarkus.samples.petclinic.services.UserSession;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

@RequestScoped
public class SessionBridge {

    @Inject
    private UserSession userSession; // Your session-scoped bean

    public UserSession getUserSession() {
        return userSession;
    }
}