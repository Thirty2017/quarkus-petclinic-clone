package org.quarkus.samples.petclinic.vet;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User extends PanacheEntityBase
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column (unique = true)
    private String username;

    @Column
    private String email;

    @Column
    private String password;


    public Long getId() { return id; }
    //getter
    public String getUsername() {
        return username;
    }
    //setter
    public void setUsername(String username) {
        this.username = username;
    }
    //getter
    public String getPassword() {
        return password;
    }
    //setter
    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return this.email;
    }
}
