package org.quarkus.samples.petclinic.services;
import at.favre.lib.crypto.bcrypt.BCrypt;
import org.quarkus.samples.petclinic.vet.User;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

@ApplicationScoped
public class UserService {

    @Inject
    EntityManager entityManager;

    private Set<User> users = new HashSet<>();
    @Transactional
    public void addUser(String username, String email, String password) {
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
        if (!email.matches(regex)) {
            throw new IllegalArgumentException("Email is not valid");
        }
        String hashedPassword = BCrypt.withDefaults().hashToString(12, password.toCharArray());
        User newUser = new User();
        newUser.setUsername(username);
        newUser.setEmail(email);
        newUser.setPassword(hashedPassword);
        entityManager.persist(newUser);
        users.add(newUser);
    }

    public User verifyUser(String email, String password) {
        User user = null;
        for (User entity : users) {
            if (entity.getEmail().equals(email)) {
                return BCrypt.verifyer().verify(password.toCharArray(), entity.getPassword()).verified ? entity : null;
            }
        }
        return null;
    }

    public User getUser(String username) {
        for (User entity : users) {
            if (entity.getUsername().equals(username)) {
                return entity;
            }
        }
        return null;
    }
}