package org.quarkus.samples.petclinic.vet;

import io.quarkus.qute.TemplateInstance;
import org.quarkus.samples.petclinic.services.UserService;
import org.quarkus.samples.petclinic.system.TemplatesLocale;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

@Path("/")
public class LoginPageResource {

    @Inject
    UserService userService;
    @Inject
    TemplatesLocale templates;


    @GET
    @Path("/login")
    public TemplateInstance loginPage(@CookieParam("userCookie") Cookie usernameCookie)
    {
        System.out.println(usernameCookie);
        if(usernameCookie == null)
            return templates.login();

        var user = userService.getUser(usernameCookie.getValue());
        return templates.welcome(user);
    }

    //verify user
    @POST
    @Path("/login")
    public Response login(@FormParam("username") String username, @FormParam("password") String password) throws URISyntaxException {
        if(username == null || password == null || username.isEmpty() || password.isEmpty())
            return Response.seeOther(new URI("/signup")).build();
        var result = userService.verifyUser(username, password);
        if(result != null)
        {
            NewCookie cookie = new NewCookie("userCookie", result.getUsername());
            return Response.seeOther(new URI("/")).cookie(cookie).build();
        }
        return Response.seeOther(new URI("/login")).build();
    }


    //sign up page
    @GET
    @Path("/signup")
    public TemplateInstance signupPage() {
        return templates.signup();
    }

    //logout
    @GET
    @Path("/logout")
    public Response logout() throws URISyntaxException {
        NewCookie cookie = new NewCookie("userCookie", "");
        return Response.seeOther(new URI("/")).cookie(cookie).build();
    }
    //create user
    @POST
    @Path("/signup")
    public TemplateInstance signup(@FormParam("username") String username, @FormParam("email") String email, @FormParam("password") String password)
    {
        userService.addUser(username, email, password);

        // Redirect to the homepage
        return templates.welcome(null);
    }
}