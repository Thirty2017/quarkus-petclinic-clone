package org.quarkus.samples.petclinic;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.quarkus.samples.petclinic.system.ErrorExceptionMapper.ERROR_HEADER;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;
import org.quarkus.samples.petclinic.vet.LoginPageResource;

@QuarkusTest
@TestHTTPEndpoint(LoginPageResource.class)
public class LoginPageResourceTest
{
    @Test
    void loginPage()
    {
        when().get("login").then()
                .statusCode(200)
                .header(ERROR_HEADER, is(nullValue()));
    }
    @Test
    void signupPage()
    {
        when().get("signup").then()
                .statusCode(200)
                .header(ERROR_HEADER, is(nullValue()));
    }
    @Test
    void logoutPage(){
        when().get("logout").then()
                .statusCode(200)
                .header(ERROR_HEADER, is(nullValue()));
    }
    @Test
    void signup()
    {
        given()
                .contentType("multipart/form-data")
                .multiPart("username", "test")
                .multiPart("email", "test@test.com")
                .multiPart("password", "test")
                .post("signup").then()
                .statusCode(200)
                .header(ERROR_HEADER, is(nullValue()));
    }

    @Test
    void login()
    {
        given()
                .contentType("multipart/form-data")
                .multiPart("username", "test2")
                .multiPart("email", "test@test.com")
                .multiPart("password", "test")
                .post("signup").then()
                .statusCode(200)
                .header(ERROR_HEADER, is(nullValue()));

        given()
                .contentType("multipart/form-data")
                .multiPart("username", "test@test.com")
                .multiPart("password", "test")
                .post("login").then()
                .statusCode(200)
                .header(ERROR_HEADER, is(nullValue()));
    }
}
