package org.quarkus.samples.petclinic.services;

import org.quarkus.samples.petclinic.vet.User;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

@SessionScoped
public class UserSession implements Serializable {
    private User user;
    @PostConstruct
    public void init() {
        // Set default values or perform other initialization
       user = null;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}